class Greeter {
    constructor(private greeting: number) { }
    greet() {
        return this.greeting;
    }
};

var greeter = new Greeter("Hello, world!");
    
console.log(greeter.greet());