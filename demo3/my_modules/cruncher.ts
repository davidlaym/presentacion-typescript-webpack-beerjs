import * as _ from 'lodash';
import {Student} from './interfaces';

export class Cruncher {
    constructor(private data: Student[]) {
        
    }
    
  getTopTen() {
      return _.chain(this.data).orderBy<Student>(['score'],['desc']).take(10).value();
  }
}


