export interface Student {
    id:number
    gender:string
    first_name:string
    last_name:string
    score:number
}