
import {Downloader} from './my_modules/downloader'
import {Cruncher} from './my_modules/cruncher'


Downloader.fetchJSONFile('MOCK_DATA.json',(data) => {
    var cruncher = new Cruncher(data);
    console.log('the top 10 are:',cruncher.getTopTen());    
});