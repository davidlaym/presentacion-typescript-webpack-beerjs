module.exports = {
    entry: "./app.ts",
    output: {
        path: __dirname,
        filename: "app.bundle.js"
    },
    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.js']
    },
    module: {
        loaders: [
             { test: /\.ts$/, loader: 'ts-loader' }
        ]
    },
    node: {
        fs: "empty"
    }
};